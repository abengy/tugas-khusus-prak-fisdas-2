<!DOCTYPE html>
<!-- Copyright Club IT Elektro UNSRI 2016
-->
<html>
    <head>
        <meta charset="UTF-8">
        <meta description="Aplikasi Tugas Khusus Prak Fisika Dasar II Club IT Teknik Elektro Universitas Sriwijaya 2016">
        <title>Aplikasi Tugas Khusus Prak Fisika Dasar II</title>
    </head>
    <link rel="stylesheet" href=<?php echo base_url('bootstrap/css/bootstrap.css');?> >
    <link rel="stylesheet" href=<?php echo base_url('bootstrap/css/bootstrap-theme.css');?> >
    <link rel="stylesheet" href=<?php echo base_url('bootstrap/css/bootstrap-theme.min.css');?> >
    <link rel="stylesheet" href=<?php echo base_url('bootstrap/css/bootstrap.min.css');?> >
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/bootstrap.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/bootstrap.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/npm.js');?>"></script>
    
    <style>
        header{
            background-color: black;
            width: 100%;
            height:auto;
            color: white;
        }
        label{
            display:block;
            margin-top: 9px;
            margin-bottom: 5px;
            
        }
        h5{
            margin-left: 2px;
        }
        h5:hover{
            color:whitesmoke;
        }
    </style>
    <body>
        <header class="main-header">
            <div id="navigation" class="clearfix">
                <h5>Tugas Khusus Fisika Dasar II v. 4</h5>
            </div>
            
        </header>
        
        <div class="container">
            <div class="main-container">
                <div class="">
                </div>
                <div class="label">
                        
                    </div>
                <?php
                    echo form_open('home/validate') ;
                ?>
                    <span class="alert-warning"><?php echo validation_errors(); ?></span>
                <?php
                    $class=array(
                        'class'=>'form-group-sm'
                    );
                    
                    $digit=array(
                        'hitam'=>'hitam',
                        'coklat'=>'coklat',
                        'merah'=>'merah',
                        'jingga'=>'jingga',
                        'kuning'=>'kuning',
                        'hijau'=>'hijau',
                        'biru'=>'biru',
                        'ungu'=>'ungu',
                        'abu-abu'=>'abu-abu',
                        'putih'=>'putih'
                    );
                    $faktor_pengali=array(
                        'emas'=>'emas',
                        'perak'=>'perak',
                        'hitam'=>'hitam',
                        'coklat'=>'coklat',
                        'merah'=>'merah',
                        'jingga'=>'jingga',
                        'kuning'=>'kuning',
                        'hijau'=>'hijau',
                        'biru'=>'biru',
                        'ungu'=>'ungu',
                    );
                    $toleransi=array(
                        'emas'=>'emas',
                        'perak'=>'perak',

                        'coklat'=>'coklat',
                        'merah'=>'merah',

                        'hijau'=>'hijau',
                        'biru'=>'biru',
                        'ungu'=>'ungu',
                    );
                    $koef_suhu=array(
                        'coklat'=>'coklat',
                        'merah'=>'merah',
                        'jingga'=>'jingga',
                        'kuning'=>'kuning'
                    );
                    echo form_label('Masukkan 3 Digit Terbelakang dari NIM anda','NIM');
                    $data=array(
                        'name'=>'NIM',
                        'type'=>'number',
                        'autofocus'=>'autofocus',
                        'required'=>'required',
                        'placeholder'=>'misal 007',
                        'class'=>'form-control'
                    );
                    echo form_input($data);
                    echo form_label('Resistor 4 Cincin','r4');

                    echo form_dropdown('r41',$digit,$class);
                    echo form_dropdown('r42',$digit);
                    echo form_dropdown('r43',$faktor_pengali);
                    echo form_dropdown('r44',$toleransi);

                    echo form_label('resistor 5 cincin','r5');
                    echo form_dropdown('r51',$digit);
                    echo form_dropdown('r52',$digit);
                    echo form_dropdown('r53',$digit);
                    echo form_dropdown('r54',$faktor_pengali);
                    echo form_dropdown('r55',$toleransi);

                    echo form_label('Resistor 6 cincin','r6');
                    echo form_dropdown('r61',$digit);
                    echo form_dropdown('r62',$digit);
                    echo form_dropdown('r63',$digit);
                    echo form_dropdown('r64',$faktor_pengali);
                    echo form_dropdown('r65',$toleransi);
                    echo form_dropdown('r66',$koef_suhu);



                    $switch_analog=array(
                        'DCV10'=>'DCV10',
                        'DCV50'=>'DCV50',
                        'DCV250'=>'DCV250',
                        'DCV1000'=>'DCV1000',
                        'DCA250m'=>'DCA250m',
                        'OHM1'=>'OHM1',
                        'OHM10'=>'OHM10',
                        'OHM100'=>'OHM100',
                        'OHM1K'=>'OHM1K',
                        'ACV10'=>'ACV10',
                        'ACV50'=>'ACV50',
                        'ACV250'=>'ACV250',
                        'ACV1000'=>'ACV1000'
                    );
                    $switch_digital=array(
                        'DCV2'=>'DCV2',
                        'DCV20'=>'DCV20',
                        'DCV200'=>'DCV200',
                        'DCV1000'=>'DCV1000',
                        'ACV2'=>'ACV2',
                        'ACV20'=>'ACV20',
                        'ACV200'=>'ACV200',
                        'ACV700'=>'ACV700',
                        'DCA200m'=>'DCA200m',
                        'ACA200m'=>'ACA200m',
                        'ACA20'=>'ACA20',
                        'OHM200'=>'OHM200',
                        'OHM2K'=>'OHM2K',
                        'OHM20K'=>'OHM20K',
                        'OHM200K'=>'OHM200K',
                    );
                    $data1=array(
                        'name'=>'angka1',
                        'type'=>'number',
                        'placeholder'=>'masukkan angka didepan koma misal 12',
                        'required'=>'required',
                        'class'=>'form-control',
                    );
                    $data2=array(
                        'name'=>'angka2',
                        'type'=>'number',
                        'placeholder'=>'Masukkan angka dibelakang koma',
                        'class'=>'form-control'
                    );
                    echo form_label('Avometer Analog','Avometer_analog');
                    echo form_dropdown('switch_analog',$switch_analog);

                    echo form_label('Avometer Digital','Avometer_Digital');
                    echo form_dropdown('switch_digital',$switch_digital); 
                    echo form_label('Masukkan angka avometer digital');
                    echo form_input($data1);
                    echo form_input($data2);
                    $tribute=array(
                        'class'=>'btn btn-success',
                        'name'=>'submit-button'
                    );
                    echo '</br>';
                    echo form_submit('submit','Cetak',$tribute);
                    
                    echo form_close();
                ?>
            </div>
            
        </div>
        <br>
        <footer>
            <div class="copyright">
                <div class="modal-footer">                   
                    <a href="#">Masuk Akal Team 2016</a>
                </div>
            </div>
        </footer>        
    </body>
</html>
