<?php

class Home extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->helper('form','array');
        $this->load->helper('url');
        $this->load->library('fpdf');
        $this->load->library('form_validation');
    }
    public function index(){
        $this->load->view('form');
    }
    public function validate(){
        if($switch_digital='DCV2'||$switch_digital='ACV2'){
            $config = array(
                            array(
                                    'field' => 'angka1',
                                    'label' => 'angka1',
                                    'rules' => 'less_than[2]'
                            ),
                            array(
                                    'field' => 'angka2',
                                    'label' => 'angka2',
                                    'rules' => 'less_than_equal_to[99]|required',
                            ),
                        );
        }
        
        if($switch_digital='DCV20' || $switch_digital='ACV20'|| $switch_digital='ACA20'||$switch_digital='OHM20K'){
            $config = array(
                            array(
                                    'field' => 'angka1',
                                    'label' => 'angka1',
                                    'rules' => 'less_than[20]|'
                            ),
                            array(
                                    'field' => 'angka2',
                                    'label' => 'angka2',
                                    'rules' => 'less_than_equal_to[9]|required'
                            ),
                        );
        }
        if($switch_digital='DCV200' || $switch_digital='ACV200'|| $switch_digital='ACA200m'||$switch_digital='OHM200'||$switch_digital='DCA200m'||$switch_digital='OHM200K'){
            $config=array(
                            array(
                                    'field' => 'angka1',
                                    'label' => 'angka1',
                                    'rules' => 'less_than[200]'
                            ),
                            array(
                                    'field' => 'angka2',
                                    'label' => 'angka2',
                                    'rules' => 'less_than_equal_to[0]'
                            ),
                        );
        }
        if($switch_digital='ACV700'){
            $config=array(
                            array(
                                    'field' => 'angka1',
                                    'label' => 'angka1',
                                    'rules' => 'less_than[700]'
                            ),
                            array(
                                    'field' => 'angka2',
                                    'label' => 'angka2',
                                    'rules' => 'less_than_equal_to[0]'
                            ),
                        );
        }
        if($switch_digital='DCV1000'){
            $config=array(
                            array(
                                    'field' => 'angka1',
                                    'label' => 'angka1',
                                    'rules' => 'less_than[1000]'
                            ),
                            array(
                                    'field' => 'angka2',
                                    'label' => 'angka2',
                                    'rules' => 'less_than_equal_to[0]'
                            ),
                        );
        }
        
        $this->form_validation->set_rules($config);
        if($this->form_validation->run() === TRUE){
            $this->produce();
        }else{
            $this->index();
        }
    }
    public function produce(){
        $NIM = $this->input->post('NIM');
        $r41=  $this->input->post('r41');
        $r42=  $this->input->post('r42');
        $r43=  $this->input->post('r43');
        $r44=  $this->input->post('r44');
        $r51=  $this->input->post('r51');
        $r52=  $this->input->post('r52');
        $r53=  $this->input->post('r53');
        $r54=  $this->input->post('r54');
        $r55=  $this->input->post('r55');
        $r61=  $this->input->post('r61');
        $r62=  $this->input->post('r62');
        $r63=  $this->input->post('r63');
        $r64=  $this->input->post('r64');
        $r65=  $this->input->post('r65');
        $r66=  $this->input->post('r66');
        $angka_riil= $this->input->post('angka1');
        $angka_desimal=  $this->input->post('angka2');
        $switch_analog= $this->input->post('switch_analog');
        $switch_digital=  $this->input->post('switch_digital');
        if ($angka_desimal==null){
            $angka_desimal='0';
        }
        $pdf = new FPDF('P','mm','A4');
        $pdf->AddPage();
        $pdf->SetCreator('Masuk Akal Team');
        $pdf->SetTitle('Tugas Khusus Fisdas II');
        $pdf->SetFont('Times','',12);
        $pdf->Cell('150');
        $pdf->Rect('160', '24', '20', '10');
        $pdf->Text('170', '30', $NIM);
        $pdf->Text('20', '42', 'Bacalah Nilai Resistor Berikut !');
        //resistor 4 cincin
        $pdf->Text('20','50','1. ');
        
        $pdf->Rect('30','50','95','15');
        switch ($r41) {
            case 'hitam':
                $pdf->SetFillColor(0, 0, 0);
                break;
            case 'coklat':
                $pdf->SetFillColor(104, 61, 9);
                break;
            case 'merah':
                $pdf->SetFillColor(225, 0, 0);
                break;
            case 'jingga':
                $pdf->SetFillColor(255, 127, 0);
                break;
            case 'kuning':
                $pdf->SetFillColor(255, 255, 0);
                break;
            case 'hijau':
                $pdf->SetFillColor(0,225,0);
                break;
            case 'biru':
                $pdf->SetFillColor(0, 0, 225);
                break;
            case 'ungu':
                $pdf->SetFillColor(162, 0, 255);
                break;
            case 'abu-abu':
                $pdf->SetFillColor(128, 128, 128);
                break;
            case 'putih':
                $pdf->SetFillColor(255, 255, 255);
                break;
            case 'emas':
                $pdf->SetFillColor(246, 244, 116);
                break;
            case 'perak':
                $pdf->SetFillColor(216,216,210);
                break;
            default:
                
                break;
        }
        $pdf->Rect('38', '50', '5', '15','DF');
        switch ($r42) {
            case 'hitam':
                $pdf->SetFillColor(0, 0, 0);
                break;
            case 'coklat':
                $pdf->SetFillColor(104, 61, 9);
                break;
            case 'merah':
                $pdf->SetFillColor(225, 0, 0);
                break;
            case 'jingga':
                $pdf->SetFillColor(255, 127, 0);
                break;
            case 'kuning':
                $pdf->SetFillColor(255, 255, 0);
                break;
            case 'hijau':
                $pdf->SetFillColor(0,225,0);
                break;
            case 'biru':
                $pdf->SetFillColor(0, 0, 225);
                break;
            case 'ungu':
                $pdf->SetFillColor(162, 0, 255);
                break;
            case 'abu-abu':
                $pdf->SetFillColor(128, 128, 128);
                break;
            case 'putih':
                $pdf->SetFillColor(255, 255, 255);
                break;
            case 'emas':
                $pdf->SetFillColor(246, 244, 116);
                break;
            case 'perak':
                $pdf->SetFillColor(216,216,210);
                break;
            default:
                
                break;
        }
        $pdf->Rect('48', '50', '5', '15','DF');
        switch ($r43) {
            case 'hitam':
                $pdf->SetFillColor(0, 0, 0);
                break;
            case 'coklat':
                $pdf->SetFillColor(104, 61, 9);
                break;
            case 'merah':
                $pdf->SetFillColor(225, 0, 0);
                break;
            case 'jingga':
                $pdf->SetFillColor(255, 127, 0);
                break;
            case 'kuning':
                $pdf->SetFillColor(255, 255, 0);
                break;
            case 'hijau':
                $pdf->SetFillColor(0,225,0);
                break;
            case 'biru':
                $pdf->SetFillColor(0, 0, 225);
                break;
            case 'ungu':
                $pdf->SetFillColor(162, 0, 255);
                break;
            case 'abu-abu':
                $pdf->SetFillColor(128, 128, 128);
                break;
            case 'putih':
                $pdf->SetFillColor(255, 255, 255);
                break;
            case 'emas':
                $pdf->SetFillColor(246, 244, 116);
                break;
            case 'perak':
                $pdf->SetFillColor(216,216,210);
                break;
            default:
                
                break;
        }
        $pdf->Rect('60', '50', '5', '15','DF');
        switch ($r44) {
            case 'coklat':
                $pdf->SetFillColor(104, 61, 9);
                break;
            case 'merah':
                $pdf->SetFillColor(225, 0, 0);
                break;
            case 'hijau':
                $pdf->SetFillColor(0,225,0);
                break;
            case 'biru':
                $pdf->SetFillColor(0, 0, 225);
                break;
            case 'ungu':
                $pdf->SetFillColor(162, 0, 255);
                break;
            case 'emas':
                $pdf->SetFillColor(246, 244, 116);
                break;
            case 'perak':
                $pdf->SetFillColor(216,216,210);
                break;
            default:
                
                break;
        }
        $pdf->Rect('92', '50', '5', '15','DF');
        $pdf->Text('30', '70', "$r41 - $r42 - $r43 - $r44");
        $pdf->Text('30','80','.................................................................................................................');
        $pdf->Text('30','90','.................................................................................................................');
        
        
        //resistor 5 cincin
        $pdf->Text('20','100','2. ');
        $pdf->Rect('30', '100','95','15');
        switch ($r51) {
            case 'hitam':
                $pdf->SetFillColor(0, 0, 0);
                break;
            case 'coklat':
                $pdf->SetFillColor(104, 61, 9);
                break;
            case 'merah':
                $pdf->SetFillColor(225, 0, 0);
                break;
            case 'jingga':
                $pdf->SetFillColor(255, 127, 0);
                break;
            case 'kuning':
                $pdf->SetFillColor(255, 255, 0);
                break;
            case 'hijau':
                $pdf->SetFillColor(0,225,0);
                break;
            case 'biru':
                $pdf->SetFillColor(0, 0, 225);
                break;
            case 'ungu':
                $pdf->SetFillColor(162, 0, 255);
                break;
            case 'abu-abu':
                $pdf->SetFillColor(128, 128, 128);
                break;
            case 'putih':
                $pdf->SetFillColor(255, 255, 255);
                break;
            case 'emas':
                $pdf->SetFillColor(246, 244, 116);
                break;
            case 'perak':
                $pdf->SetFillColor(216,216,210);
                break;
            default:
                
                break;
        }
        
        $pdf->Rect('38', '100', '5', '15','DF');
        switch ($r52) {
            case 'hitam':
                $pdf->SetFillColor(0, 0, 0);
                break;
            case 'coklat':
                $pdf->SetFillColor(104, 61, 9);
                break;
            case 'merah':
                $pdf->SetFillColor(225, 0, 0);
                break;
            case 'jingga':
                $pdf->SetFillColor(255, 127, 0);
                break;
            case 'kuning':
                $pdf->SetFillColor(255, 255, 0);
                break;
            case 'hijau':
                $pdf->SetFillColor(0,225,0);
                break;
            case 'biru':
                $pdf->SetFillColor(0, 0, 225);
                break;
            case 'ungu':
                $pdf->SetFillColor(162, 0, 255);
                break;
            case 'abu-abu':
                $pdf->SetFillColor(128, 128, 128);
                break;
            case 'putih':
                $pdf->SetFillColor(255, 255, 255);
                break;
            case 'emas':
                $pdf->SetFillColor(246, 244, 116);
                break;
            case 'perak':
                $pdf->SetFillColor(216,216,210);
                break;
            default:
                
                break;
        }
        
        $pdf->Rect('48', '100', '5', '15','DF');
        switch ($r53) {
            case 'hitam':
                $pdf->SetFillColor(0, 0, 0);
                break;
            case 'coklat':
                $pdf->SetFillColor(104, 61, 9);
                break;
            case 'merah':
                $pdf->SetFillColor(225, 0, 0);
                break;
            case 'jingga':
                $pdf->SetFillColor(255, 127, 0);
                break;
            case 'kuning':
                $pdf->SetFillColor(255, 255, 0);
                break;
            case 'hijau':
                $pdf->SetFillColor(0,225,0);
                break;
            case 'biru':
                $pdf->SetFillColor(0, 0, 225);
                break;
            case 'ungu':
                $pdf->SetFillColor(162, 0, 255);
                break;
            case 'abu-abu':
                $pdf->SetFillColor(128, 128, 128);
                break;
            case 'putih':
                $pdf->SetFillColor(255, 255, 255);
                break;
            case 'emas':
                $pdf->SetFillColor(246, 244, 116);
                break;
            case 'perak':
                $pdf->SetFillColor(216,216,210);
                break;
            default:
                
                break;
        }
        
        $pdf->Rect('60', '100', '5', '15','DF');
        switch ($r54) {
            case 'hitam':
                $pdf->SetFillColor(0, 0, 0);
                break;
            case 'coklat':
                $pdf->SetFillColor(104, 61, 9);
                break;
            case 'merah':
                $pdf->SetFillColor(225, 0, 0);
                break;
            case 'jingga':
                $pdf->SetFillColor(255, 127, 0);
                break;
            case 'kuning':
                $pdf->SetFillColor(255, 255, 0);
                break;
            case 'hijau':
                $pdf->SetFillColor(0,225,0);
                break;
            case 'biru':
                $pdf->SetFillColor(0, 0, 225);
                break;
            case 'ungu':
                $pdf->SetFillColor(162, 0, 255);
                break;
            case 'abu-abu':
                $pdf->SetFillColor(128, 128, 128);
                break;
            case 'putih':
                $pdf->SetFillColor(255, 255, 255);
                break;
            case 'emas':
                $pdf->SetFillColor(246, 244, 116);
                break;
            case 'perak':
                $pdf->SetFillColor(216,216,210);
                break;
            default:
                
                break;
        }
        $pdf->Rect('73', '100', '5', '15','DF');
        switch ($r55) {
            case 'coklat':
                $pdf->SetFillColor(104, 61, 9);
                break;
            case 'merah':
                $pdf->SetFillColor(225, 0, 0);
                break;
            case 'hijau':
                $pdf->SetFillColor(0,225,0);
                break;
            case 'biru':
                $pdf->SetFillColor(0, 0, 225);
                break;
            case 'ungu':
                $pdf->SetFillColor(162, 0, 255);
                break;
            case 'emas':
                $pdf->SetFillColor(246, 244, 116);
                break;
            case 'perak':
                $pdf->SetFillColor(216,216,210);
                break;
            default:
                
                break;
        }
        $pdf->Rect('93', '100', '5', '15','DF');
        $pdf->Text('30','130','.................................................................................................................');
        $pdf->Text('30','140','.................................................................................................................');
        $pdf->Text('30', '120', "$r51 - $r52 - $r53 - $r54 - $r55");
        //resistor 6 cincin
        $pdf->Text('20','150','3. ');
        $pdf->Rect('30', '150','95','15');
        switch ($r61) {
            case 'hitam':
                $pdf->SetFillColor(0, 0, 0);
                break;
            case 'coklat':
                $pdf->SetFillColor(104, 61, 9);
                break;
            case 'merah':
                $pdf->SetFillColor(225, 0, 0);
                break;
            case 'jingga':
                $pdf->SetFillColor(255, 127, 0);
                break;
            case 'kuning':
                $pdf->SetFillColor(255, 255, 0);
                break;
            case 'hijau':
                $pdf->SetFillColor(0,225,0);
                break;
            case 'biru':
                $pdf->SetFillColor(0, 0, 225);
                break;
            case 'ungu':
                $pdf->SetFillColor(162, 0, 255);
                break;
            case 'abu-abu':
                $pdf->SetFillColor(128, 128, 128);
                break;
            case 'putih':
                $pdf->SetFillColor(255, 255, 255);
                break;
            case 'emas':
                $pdf->SetFillColor(255, 215, 0);
                break;
            case 'perak':
                $pdf->SetFillColor(216,216,210);
                break;
            default:
                
                break;
        }
        $pdf->Rect('38', '150', '5', '15','DF');
        switch ($r62) {
            case 'hitam':
                $pdf->SetFillColor(0, 0, 0);
                break;
            case 'coklat':
                $pdf->SetFillColor(104, 61, 9);
                break;
            case 'merah':
                $pdf->SetFillColor(225, 0, 0);
                break;
            case 'jingga':
                $pdf->SetFillColor(255, 127, 0);
                break;
            case 'kuning':
                $pdf->SetFillColor(255, 255, 0);
                break;
            case 'hijau':
                $pdf->SetFillColor(0,225,0);
                break;
            case 'biru':
                $pdf->SetFillColor(0, 0, 225);
                break;
            case 'ungu':
                $pdf->SetFillColor(162, 0, 255);
                break;
            case 'abu-abu':
                $pdf->SetFillColor(128, 128, 128);
                break;
            case 'putih':
                $pdf->SetFillColor(255, 255, 255);
                break;
            case 'emas':
                $pdf->SetFillColor(255, 215, 0);
                break;
            case 'perak':
                $pdf->SetFillColor(216,216,210);
                break;
            default:
                
                break;
        }
        $pdf->Rect('48', '150', '5', '15','DF');
        switch ($r63) {
            case 'hitam':
                $pdf->SetFillColor(0, 0, 0);
                break;
            case 'coklat':
                $pdf->SetFillColor(104, 61, 9);
                break;
            case 'merah':
                $pdf->SetFillColor(225, 0, 0);
                break;
            case 'jingga':
                $pdf->SetFillColor(255, 127, 0);
                break;
            case 'kuning':
                $pdf->SetFillColor(255, 255, 0);
                break;
            case 'hijau':
                $pdf->SetFillColor(0,225,0);
                break;
            case 'biru':
                $pdf->SetFillColor(0, 0, 225);
                break;
            case 'ungu':
                $pdf->SetFillColor(162, 0, 255);
                break;
            case 'abu-abu':
                $pdf->SetFillColor(128, 128, 128);
                break;
            case 'putih':
                $pdf->SetFillColor(255, 255, 255);
                break;
            case 'emas':
                $pdf->SetFillColor(246, 244, 116);
                break;
            case 'perak':
                $pdf->SetFillColor(216,216,210);
                break;
            default:
                
                break;
        }
        
        $pdf->Rect('60', '150', '5', '15','DF');
        switch ($r64) {
            case 'hitam':
                $pdf->SetFillColor(0, 0, 0);
                break;
            case 'coklat':
                $pdf->SetFillColor(104, 61, 9);
                break;
            case 'merah':
                $pdf->SetFillColor(225, 0, 0);
                break;
            case 'jingga':
                $pdf->SetFillColor(255, 127, 0);
                break;
            case 'kuning':
                $pdf->SetFillColor(255, 255, 0);
                break;
            case 'hijau':
                $pdf->SetFillColor(0,225,0);
                break;
            case 'biru':
                $pdf->SetFillColor(0, 0, 225);
                break;
            case 'ungu':
                $pdf->SetFillColor(162, 0, 255);
                break;
            case 'abu-abu':
                $pdf->SetFillColor(128, 128, 128);
                break;
            case 'putih':
                $pdf->SetFillColor(255, 255, 255);
                break;
            case 'emas':
                $pdf->SetFillColor(246, 244, 116);
                break;
            case 'perak':
                $pdf->SetFillColor(216,216,210);
                break;
            default:
                
                break;
        }
        $pdf->Rect('73', '150', '5', '15','DF');
        switch ($r65) {
            case 'coklat':
                $pdf->SetFillColor(104, 61, 9);
                break;
            case 'merah':
                $pdf->SetFillColor(225, 0, 0);
                break;
            case 'hijau':
                $pdf->SetFillColor(0,225,0);
                break;
            case 'biru':
                $pdf->SetFillColor(0, 0, 225);
                break;
            case 'ungu':
                $pdf->SetFillColor(162, 0, 255);
                break;
            case 'emas':
                $pdf->SetFillColor(246, 244, 116);
                break;
            case 'perak':
                $pdf->SetFillColor(216,216,210);
                break;
            default:
                
                break;
        }
        $pdf->Rect('90', '150', '5', '15','DF');
        switch ($r66) {
            case 'coklat':
                $pdf->SetFillColor(104, 61, 9);
                break;
            case 'merah':
                $pdf->SetFillColor(225, 0, 0);
                break;
            case 'jingga':
                $pdf->SetFillColor(255, 127, 0);
                break;
            case 'kuning':
                $pdf->SetFillColor(255, 255, 0);
                break;
            default:
                
                break;
        }
        $pdf->Rect('110', '150', '5', '15','DF');
        $pdf->Text('30','180','.................................................................................................................');
        $pdf->Text('30','190','.................................................................................................................');
        $pdf->Text('30', '170', "$r61 - $r62 - $r63 - $r64 - $r65 - $r66");
        //$pdf->Image('http://localhost/tkf2/asset/1.jpg','60','30','90','0');
        $pdf->AddPage();
        $pdf->Cell('150');
        $pdf->Rect('160', '24', '20', '10');
        $pdf->Text('170', '30', $NIM);
        $pdf->Text('20', '42', 'Bacalah Hasil Ukur Berikut !');
       
        //avometer analog
        $pdf->Text('20','50','4. ');
        $pdf->Rect('30', '50', '60', '70');
        if($switch_analog===null){
            $file= base_url("/asset/analog.jpg");
        }else{
            $file= base_url("/asset/analog/$switch_analog.jpg");
        }
        $pdf->Image($file, '30', '50', '60', '70') ;
        $pdf->Text('30','130','.................................................................................................................');
        $pdf->Text('30','140','.................................................................................................................');
        
        
        //avometer digital
        $pdf->Text('20','150','5. ');
        $pdf->Rect('30', '150', '60', '70');
        if($switch_digital===null){
            $file1= base_url("/asset/digital.jpg");
        }else{
            $file1= base_url("/asset/digital/$switch_digital.jpg");
        }
        $pdf->Image($file1, '30', '150', '60', '75','jpg') ;
        $pdf->Text('65', '163', "$angka_riil,$angka_desimal");
        $pdf->Text('30','230','.................................................................................................................');
        $pdf->Text('30','240','.................................................................................................................');
        $pdf->Output();
    }
}